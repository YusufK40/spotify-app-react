import React from 'react'
import logo from '../img/logo.png'

const AUTH_URL =
  'https://accounts.spotify.com/authorize?client_id=fdae10466fa14216ad1800aa9ec009b3&response_type=code&redirect_uri=http://localhost:3000&scope=streaming%20user-read-email%20user-read-private%20user-library-read%20user-library-modify%20user-read-playback-state%20user-modify-playback-state'

export default function Login () {
  return (
    <div className='flex bg-gray-900 h-screen justify-center'>
      <div className='flex justify-center items-center'>
        <div
          id='login'
          className='p-8 bg-gradient-to-br
              from-gray-800 to-gray-700 rounded-lg w-96 pb-10'
        >
          <div className='flex justify-center mb-4'>
            <img className='logo' src={logo} alt='logo' width='70' />
          </div>

          <a
            id='loginButton'
            href={AUTH_URL}
            type='button'
            className='flex justify-center items-center h-12 mt-3 text-white w-full rounded bg-green-700 hover:bg-green-800 transition-all'
          >
            Login to Spotify
          </a>
        </div>
      </div>
    </div>
  )
}
