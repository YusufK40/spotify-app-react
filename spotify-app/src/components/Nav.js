import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'

const Nav = ({ searchStatus, setSearchStatus }) => {
  return (
    <nav id='nav' className='flex h-20 justify-around items-center'>
      <h1 className='text-2xl font-bold text-green-600'>Spotify</h1>
      <button
        id='searchBtn'
        onClick={() => setSearchStatus(!searchStatus)}
        className='bg-tranparent cursor-pointer text-white border-2 rounded border-green-600 p-1 transition-all  hover:bg-green-600 hover:text-white'
      >
        Search
        <FontAwesomeIcon className='pl-2' icon={faSearch} />
      </button>
    </nav>
  )
}

export default Nav
